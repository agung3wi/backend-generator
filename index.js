const express = require('express')
const app = express()
const port = 9500
var cors = require('cors')
const shell = require('shelljs');
const fs = require("fs");
const path = require("path");
const { exec, spawn } = require('child_process');
const _ = require('lodash');

app.use(cors());
app.use(express.json({ limit: '50mb' }));
// app.use(bodyParser.json({ limit: '50mb' }));

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/generate', async (req, res) => {
    let xmlParser = require('xml2json');
    let xmlString = `<?xml version="1.0" encoding="UTF-8"?>
<TestScenario>
   <TestSuite name="TS_EdgeHome">
      <TestCaseName name="tc_Login">dt_EdgeCaseHome,dt_EdgeCaseRoute</TestCaseName>
      <TestCaseName name="tc_Logout">dt_EdgeCaseRoute</TestCaseName>
   </TestSuite>
   <TestSuite name="TS_EdgePanel">
      <TestCaseName name="tc_AddContract">dt_EdgeCaseHome,dt_EdgeCaseSpectrum</TestCaseName>
   </TestSuite>
      <TestSuite name="TS_EdgeRoute">
      <TestCaseName name="tc_VerifyContract">dt_EdgeCaseRoute</TestCaseName>
      <TestCaseName name="tc_Payment">dt_EdgeCaseRoute</TestCaseName>
   </TestSuite>
   <TestSuite name="TS_EdgeSpectrum">
      <TestCaseName name="tc_ClientFeedback">dt_EdgeCaseSpectrum</TestCaseName>
   </TestSuite>
</TestScenario>`;

    return res.json(JSON.parse(xmlParser.toJson(xmlString)));
})

var saveFile = function (BasePath, tmp, scope) {
    try {
        for (let p in scope) {
            eval("var " + p + " = scope[p];");
        }
        eval("var filename = `" + tmp.file_name + "`")
        var _ = require('lodash');
        var abjadList = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        if (tmp.type_template != 'generate') {
            eval("var content = `" + tmp.file_content + "`")
            var beforeContent = fs.existsSync(BasePath + filename) ? String(fs.readFileSync(BasePath + filename)) : "";

            if ((tmp.after !== null && tmp.after !== undefined && tmp.after !== "")
                && (tmp.before !== null && tmp.before !== undefined && tmp.before !== "")) {

                var regExString = new RegExp("(?:" + tmp.after + ")(.*?)(?:" + tmp.before + ")", "igs"); //set ig flag for global search and case insensitive

                var testRE = regExString.exec(beforeContent);
                if (testRE && testRE.length > 1) //RegEx has found something and has more than one entry.
                {
                    content = beforeContent.replace(tmp.after + testRE[1] + tmp.before, tmp.after + content + tmp.before);
                } else {
                    content = beforeContent;
                }
            } else if (tmp.after !== null && tmp.after !== undefined && tmp.after !== "") {
                content = beforeContent.replace(tmp.after, tmp.after + content);
            } else if (tmp.before !== null && tmp.before !== undefined && tmp.before !== "") {
                content = beforeContent.replace(tmp.before, content + tmp.before);
            } else {
                content = beforeContent;
            }

        } else {
            eval("var content = `" + tmp.file_content + "`")
        }
        var dir = path.dirname(BasePath + filename);
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true });
        }
        fs.writeFileSync(BasePath + filename, content)
        console.log(`file ${filename} is generated`)
    } catch (err) {
        console.log(err)
        console.log(`cannot generate file ${filename}`);
    }
}

var generate = function (jsonContent, BasePath, tmpFor) {
    // var jsonContent = {};
    // if (fs.existsSync(pathFileJson)) {
    //     var content = fs.readFileSync(pathFileJson);
    //     jsonContent = JSON.parse(content);
    // } else {
    //     throw "File json template not found"
    // }
    var templates = jsonContent.template.filter(x => x.template_for == tmpFor);
    console.log("Generate tables ...");
    var tables = jsonContent.tables;
    for (let i = 0; i < tables.length; i++) {
        var table = tables[i];

        templates.forEach(tmp => {
            if (tmp.type_module == 'table') {
                saveFile(BasePath, tmp, table);
            }
        });

    }

    console.log("Generate modules ...");
    var modules = jsonContent.modules;
    for (let i = 0; i < modules.length; i++) {
        var module = modules[i];
        var table = jsonContent.tables.find(x => x.table_name == module.table_name);

        var uniques = [];
        var primary_key = [];

        for (let a = 0; a < table.primary_key.length; a++) {
            primary_key.push(_.upperFirst(_.camelCase(table.primary_key[a])));
        }

        module.primary_key = table.primary_key;
        module.primary_key_new = primary_key;
        module.hasActive = false;

        for (let b = 0; b < table.columns.length; b++) {
            const column = table.columns[b];
            if (column.column_name == 'active')
                module.hasActive = true;
        }

        for (let j = 0; j < module.indexes.length; j++) {
            const index = module.fields[y];
            if (index.unique) {
                uniques.push(index.columns)
            }
        }

        for (let k = 0; k < module.fields.length; k++) {
            const field = module.fields[k];
            if (field.field_name == 'active')
                module.hasActive = true;
            if (field.unique) {
                uniques.push([field.field_name])
            }
        }

        module.camel_case = _.camelCase(module.module_name)
        module.studly_caps = _.upperFirst(_.camelCase(module.module_name))
        module.kebab_case = _.kebabCase(module.module_name)
        module.snake_case = module.module_name



        var uniquesNew = [];

        for (let l = 0; l < uniques.length; l++) {
            var columnNew = [];
            for (let m = 0; m < uniques[l].length; m++) {
                columnNew.push(_.upperFirst(_.camelCase(uniques[l])));
            }

            var validateData = {
                module: module.module,
                _: _,
                columns: uniques[l],
                column_new: columnNew
            }

            uniquesNew.push(columnNew);
        }


        module.unique_new = uniquesNew;

        templates.forEach(tmp => {
            if (tmp.type_module == 'module' && !module.lock) {
                saveFile(BasePath, tmp, module);
            }
        });

    }

    templates.forEach(tmp => {
        if (tmp.type_module == 'list_module') {
            saveFile(BasePath, tmp, jsonContent);
        }
    });

}

app.post('/generate', async (req, res) => {
    try {
        var input = req.body,
            config = input.config;

        var response = {
            success: true
        }
        // var fileProject = __dirname + '/project.json';
        // fs.writeFileSync(fileProject, JSON.stringify(config));

        if (config.template.findIndex(item => item.template_for == 'back') > -1) {

            if (!fs.existsSync(input.back_path)) {
                response.success = false;
                response.error_message = "Directory backend doesn't exists";
                return res.json(response, 422)
            } else {
                generate(config, input.back_path, 'back');
            }
        }

        if (config.template.findIndex(item => item.template_for == 'front') > -1) {

            if (!fs.existsSync(input.front_path)) {
                response.success = false;
                response.error_message = "Directory frontend doesn't exists";
                return res.json(response, 422)
            } else {
                generate(config, input.front_path, 'front');
            }
        }

        if (config.template.findIndex(item => item.template_for == 'mobile') > -1) {

            if (!fs.existsSync(input.mobile_path)) {
                response.success = false;
                response.error_message = "Directory mobile doesn't exists";
                return res.json(response, 422)
            } else {
                generate(config, input.mobile_path, 'mobile');
            }
        }


        return res.json(response);

    } catch (err) {
        console.log(err)
        response.success = false;
        response.error_message = err;
        return res.json(response, 500)
    }


})

app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`))
